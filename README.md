# 停车场系统小程序

#### 介绍
功能都能正常使用的小程序

 **【之前的公司离职了，我现在免费分享，成品效果展示】:** 

![输入图片说明](gh_421c73099590_258%20(1).jpg)

最近受到不少反馈说不一样，估计是原来的公司修改界面UI了，大家不要在意这些细节，底层接口请求和业务逻辑是没变的

【 **互动交流** 】：本代码完整且可正常编译运行，没有任何代码缺失，如果无法跑起来的同学请先补一下小程序开发的基础知识，业余爱好分享，希望能对各位提供一定的帮助，若需不懂的地方可以加我微信: **Dove981011512** ，小白和没开发经验的就不要加我了，教基础的东西太难了，再次跟您说声对不起

【开发建议】：想研究相机对接的，建议自己买个开发机器，连个两三百的开发机器你都没有，不要谈啥研究车牌识别系统。先声明，我自己不是推销产品啥的，根据个人需求，需要购买的自己去某宝购买，连接我给在这：

【单个开发板】：https://item.taobao.com/item.htm?spm=a21dvs.23580594.0.0.52de3d0drQZjAD&ft=t&id=684813553506 (自己记得再买个12V电源)

【整机】：https://item.taobao.com/item.htm?spm=a21dvs.23580594.0.0.52de3d0drQZjAD&ft=t&id=671971812765 (这个已经带电源了)


#### 安装教程

1.  下载该代码到你自己的电脑上
2.  使用HBuilder编辑器打开项目，如果你电脑上没有该编辑工具请下载：https://www.dcloud.io/hbuilderx.html
3.  找到parking/pages/index/index/vue
4.  配置好的Hbuilder，例如设置好微信开发者工具的路径，小程序appid等
5.  ![输入图片说明](https://images.gitee.com/uploads/images/2020/0805/111316_2969e02c_753802.png "微信图片_20200805111256.png")
6. ![输入图片说明](https://images.gitee.com/uploads/images/2020/0813/141856_3dae67fa_2176643.png "微信截图_20200805111922.png")
7. ![输入图片说明](https://images.gitee.com/uploads/images/2020/0813/141914_1301e7eb_2176643.png "微信图片_20200805111256.png")


#### 使用说明

1.  服务端安装教程：https://www.showdoc.cc/902821218399318?page_id=4811879051734217
2.  后台管理设置好自己的相关微信账号
3.  直接使用
4.  服务端：https://gitee.com/wangdefu/caraprk_public

备注：服务端只是基础框架，需要完善的功能请个人自行完善接口开发，祝您工作愉快

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
